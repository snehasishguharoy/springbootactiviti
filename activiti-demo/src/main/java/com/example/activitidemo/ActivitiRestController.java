package com.example.activitidemo;


import org.activiti.engine.RuntimeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ActivitiRestController {

    @Autowired
    private RuntimeService runtimeService;


    @RequestMapping(value = "/start-my-process", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public String startMyProcess() {
        runtimeService.startProcessInstanceByKey("TestProcess");
        String message = "We have now" + runtimeService.createProcessInstanceQuery().count() + "process instances";
        System.out.println("We have now" + runtimeService.createProcessInstanceQuery().count() + "process instances");
        return message;

    }
}
