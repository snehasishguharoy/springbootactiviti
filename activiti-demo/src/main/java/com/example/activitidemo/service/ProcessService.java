package com.example.activitidemo.service;

import com.example.activitidemo.entity.Employee;
import com.example.activitidemo.repo.EmployeeRepo;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.task.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ProcessService {

    @Autowired
    private EmployeeRepo employeeRepo;
    @Autowired
    private RuntimeService runtimeService;
    @Autowired
    private TaskService taskService;
    @Autowired
    private RepositoryService repositoryService;

    // start the process and set employee as variable
    public String startTheProcess(String assignee) {
        Employee employee = employeeRepo.findByName(assignee);
        Map<String, Object> variables = new HashMap<>();
        variables.put("employee", employee);
        runtimeService.startProcessInstanceByKey("simple-process", variables);
        return processInformation();


    }

    // fetching process and task information
    private String processInformation() {
        List<Task> taskList = taskService.createTaskQuery().orderByTaskCreateTime().asc().list();
        StringBuilder processAndTaskInfo = new StringBuilder();
        processAndTaskInfo.append("Number of process definition available: "
                + repositoryService.createProcessDefinitionQuery().count() + " | Task Details= ");

        taskList.forEach(task -> {
            processAndTaskInfo.append("ID: " + task.getId() + ", Name: " + task.getName() + ", Assignee: "
                    + task.getAssignee() + ", Description: " + task.getDescription());
        });
        return processAndTaskInfo.toString();

    }

    // fetch task assigned to employee
    public List<Task> getTasks(String name) {
        return taskService.createTaskQuery().taskAssignee(name).list();
    }
    // complete the task
    public void completeTask(String taskId) {
        taskService.complete(taskId);
    }


}
