package com.example.activitidemo.repo;

import com.example.activitidemo.entity.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmployeeRepo extends JpaRepository<Employee,Long> {
    public Employee findByName(String name);
}
